#include "baseprotocol.h"
#include "debug.h"

using namespace tvlite;
CBaseProtocol::CBaseProtocol(wxEvtHandler *parent, wxString url, wxString cmd):
                  wxProcess(parent, wxID_ANY),
                  m_pparent(parent),
                  m_url(url),
                  //m_name(name),
                  m_cmd(cmd)
{

}

CBaseProtocol::~CBaseProtocol()
{
}

///////////////////////////////////////////////////////////////////////////

wxString CBaseProtocol::GetURL()
{
   return m_url;
};

///////////////////////////////////////////////////////////////////////////

void CBaseProtocol::SetURL(wxString url)
{
   m_url = url;
}

///////////////////////////////////////////////////////////////////////////

wxString CBaseProtocol::GetName()
{
   return m_name;
};

///////////////////////////////////////////////////////////////////////////

void CBaseProtocol::SetName(wxString name)
{
   m_name = name;
}

///////////////////////////////////////////////////////////////////////////

wxString CBaseProtocol::GetCmd()
{
   return m_cmd;
}

///////////////////////////////////////////////////////////////////////////

void CBaseProtocol::SetCmd(wxString cmd)
{
   m_cmd = cmd;
}

///////////////////////////////////////////////////////////////////////////
void CBaseProtocol::OnTerminate(int pid, int status)
{

   DBG_INFO("On Terminate entered");
   DBG_INFO("Process %u ('%s') terminated with exit code %d.",
                pid, (const char*)m_cmd.c_str(), status);
   wxProcess::OnTerminate(pid,status);
}

void CBaseProtocol::StopProtocol()
{
     DBG_INFO("Enter stop protocol");
     if (Exists(this->GetPid()))
     {
        DBG_INFO("Killing current process");
        wxProcess::Kill((int)this->GetPid(), wxSIGKILL);
     }
     DBG_INFO("Exit stop protocol");
}

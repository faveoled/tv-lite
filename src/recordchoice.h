#ifndef CRECORDCHOICE_H
#define CRECORDCHOICE_H

#include <wx/wx.h>
#include <wx/window.h>

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif

namespace tvlite
{

class CRecordChoice : public CRecordChoiceBase
{
private:
   CRecordChoice(const CRecordChoice& rhs);
   CRecordChoice& operator=(const CRecordChoice& rhs);

public:
   CRecordChoice();
   CRecordChoice(wxWindow *parent);
   ~CRecordChoice();
  
   virtual void OnLeaveRadioBox( wxFocusEvent& event ); 
#ifdef __WXMSW__   
   virtual void OnClickRadioBox( wxCommandEvent& event);
#endif   
   void SetSelection(int selection);
};

}

#endif // CRECORDCHOICE_H

#ifndef CHANNELLISTCTRL_H
#define CHANNELLISTCTRL_H

#include <wx/wx.h>
#include <wx/listctrl.h>
#include "channel.h"

using namespace tvlite;

class channelListCtrl : public wxListCtrl
{
public:
   channelListCtrl(wxWindow *parent, wxWindowID id, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=wxLC_ICON,
                  const wxValidator &validator=wxDefaultValidator, const wxString &name=wxListCtrlNameStr);
   virtual ~channelListCtrl();
   void SetChannelList(TPChannelList *channelList);
protected:
   virtual wxString OnGetItemText (long item, long column) const;
private:
   TPChannelList *m_channelList;

};

#endif // CHANNELLISTCTRL_H

#ifndef CTEXTWRAPPER_H
#define CTEXTWRAPPER_H

#include <wx/textwrapper.h>
namespace tvlite
{

class CTextWrapper:public wxTextWrapper
{
public:
   CTextWrapper(wxWindow *win, int widthMax);
   CTextWrapper() = delete;
   ~CTextWrapper();
   wxString Wrap(wxString &inputText);
protected:
   virtual void OnOutputLine(const wxString& line);
   virtual void OnNewLine();
private:
   wxString m_wrappedText;
   wxWindow *m_win;
   int m_widthMax;
   
};
   
}


#endif // CTEXTWRAPPER_H

#ifndef CSTATUSGAUGE_H
#define CSTATUSGAUGE_H

#include <wx/dynarray.h>
#include <wx/statusbr.h>
#include <wx/gauge.h>

namespace tvlite
{

enum EStatusGauge
{
   E_SB_LISTBUFFER   = 100,
   E_SB_LISTPROGRESS = 101
};
   
   
class CStatusGauge;

WX_DECLARE_OBJARRAY(CStatusGauge*, TStatusGaugeList);    
   
class CStatusGauge
{
   protected:
   int m_id;
   wxStatusBar  *m_parent;
   int m_captionPane;
   int m_gaugePane;
   wxGauge  *m_gauge;
  
   public:
   CStatusGauge() = delete;
   CStatusGauge(int id, wxStatusBar* parent, int captionPane);
   virtual ~CStatusGauge();
//static members
   static TStatusGaugeList m_list;
   static void CreateStatusGauge(wxStatusBar *parent, int id, int captionPane);
   static void DestroyStatusGauges();
   static CStatusGauge* GetStatusGauge(int id); 
   static void ShowStatusGauge(int id, wxString caption, bool visibleGauge = true);
   static void HideStatusGauge(int id);
   static bool IsVisible(int id);
   static void RedrawStatusGauges(wxStatusBar *parent);
   static void UpdateGauge(int id, int data);

};



}

#endif // CSTATUSGAUGE_H

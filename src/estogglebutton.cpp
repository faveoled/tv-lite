#include "estogglebutton.h"

CESToggleButton::CESToggleButton (wxWindow *parent, 
         wxWindowID id, 
         const wxString &label, 
         const wxPoint &pos,
         const wxSize &size,
         long style,
         const wxValidator &val, 
         const wxString &name):wxToggleButton(parent, id, label, pos, size, wxBU_EXACTFIT, val, name)
{
}

CESToggleButton::~CESToggleButton()
{
}

